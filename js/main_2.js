$(function(){
    
    var isff=false;
    
    if(navigator.userAgent.toLowerCase().indexOf('firefox') > -1)
    {
         isff=true;
    }

    var transforms = false;
    var touch = false;
    
    // Detecting IE
    var oldIE;
    if ($('html').is('.ie6, .ie7, .ie8, .ie9')) {
        oldIE = true;
    }

    if (oldIE) {

    } else {
        
    }
    
    if (Modernizr.csstransforms3d) {
      // CSS 3D Transforms supported
        transforms=true;
    } else {
      // not supported
    }
    
    if (Modernizr.touch) {
      // CSS 3D Transforms supported
        touch=true;
    } else {
      // not supported
    }
    
    //hide nav//
    $('.menu').css('display','none');

    window.onload=function(){
        TweenMax.from($('.line'),2,{height:0, ease:Strong.easeOut, delay:0.3});
        TweenMax.delayedCall(0.8,function(){
            $('.intro-text-holder').css('display','block');
            if(!isff){
                TweenMax.from($('.intro-text-holder'),1,{left:-460});
            }else{
                TweenMax.from($('.intro-text-holder'),2,{opacity:0, ease:Power3.easeInOut});
            }
            
            TweenMax.to($('.line'),1.5,{left:-1750,delay:0,ease:Strong.easeIn, delay:0.1});
            TweenMax.to($('.line'),0.5,{opacity:0, delay:2.5});

            TweenMax.to($('.fcb-office'),1,{marginLeft:0, delay:0.75});
            
            
            TweenMax.delayedCall(4,function(){
               $('.menu').css('display','block');
               TweenMax.to($('.intro'),1,{marginLeft:'100%', width:0, delay:1, ease:Strong.easeIn});
               TweenMax.to($('.fcb-office'),1,{marginLeft:'-100%', delay:1, ease:Strong.easeIn});
            });
        });
        
        if(!touch){
            $('.menu-box').mouseenter(function(e){
                var mc=$(this).find('.overlay');
                var img = $(mc).find('.menu-line');
                var title = $(mc).find('.menu-title');
                var desc = $(mc).find('p');
                TweenMax.to(mc,1,{marginLeft:'100%', ease:Strong.easeIn});
            });

            $('.menu-box').mouseleave(function(e){
                var mc=$(this).find('.overlay');
                var img = $(mc).find('.menu-line');
                var title = $(mc).find('.menu-title');
                var desc = $(mc).find('p');
                TweenMax.to(mc,1,{marginLeft:'0', ease:Strong.easeOut});
            });
        }else{
            $('.menu-box').click(function(e){
                var mc=$(this).find('.overlay');
                var img = $(mc).find('.menu-line');
                var title = $(mc).find('.menu-title');
                var desc = $(mc).find('p');
                TweenMax.to(mc,1,{marginLeft:'100%', ease:Strong.easeIn});
            });
        }
    }
    
    window.onresize = function(event) {
        //$('.intro').css('height',$(window).height());
    };
        
});