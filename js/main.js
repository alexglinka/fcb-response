$(document).ready(function(){
  
});



var case_studies=["kmart","oreo","sunlife"];
var creative_work=["td","kraft"];
var tv_work=["kmart_tv","coors","weight_watchers","kraft_tv","pet","dow"];

var section="";
var selectedPerson;
var teamEvents;

$(function(){
    $('.ui-loader').hide();
    
    var isff=false;
    var modalOpen=false;
    var loader={frame:0};
    
    //loader animation//
    $('.line').css('display','none');
    TweenMax.to(loader,2,{frame:25, onUpdate:checkIt, repeat:-1, ease:Linear.easeNone});
    
    function checkIt(){
        var framenum=(Math.floor(loader.frame)*50);
        $('.loader img').css('margin-top','-'+(framenum)+'px');
    }
    
    if(navigator.userAgent.toLowerCase().indexOf('firefox') > -1)
    {
         isff=true;
    }

    var transforms = false;
    var touch = false;
    
    // Detecting IE
    var oldIE;
    if ($('html').is('.ie6, .ie7, .ie8, .ie9')) {
        oldIE = true;
    }

    if (oldIE) {

    } else {
        
    }
    
    if (Modernizr.csstransforms3d) {
      // CSS 3D Transforms supported
        transforms=true;
    } else {
      // not supported
    }
    
    if (Modernizr.touch) {
      // CSS 3D Transforms supported
        touch=true;
    } else {
      // not supported
    }
    
    //hide nav//
    $('.menu').css('display','none');

    window.onload=function(){
        $('.case-wrapper').imagesLoaded( function() {
            $('.line').css('display','block');
            $('.loader').fadeOut();
            TweenMax.from($('.line'),2,{height:0, ease:Strong.easeOut, delay:0.3});
            //return;
            TweenMax.delayedCall(0.8,function(){
                $('.intro-text-holder').css('display','block');
                if(!isff){
                    TweenMax.from($('.intro-text-holder'),1,{left:-460});
                }else{
                    TweenMax.from($('.intro-text-holder'),2,{opacity:0, ease:Power3.easeInOut});
                }
                TweenMax.to($('.line'),1.5,{left:-1750,delay:0,ease:Strong.easeIn, delay:0.1});
                TweenMax.to($('.line'),0.5,{opacity:0, delay:2.5});

                TweenMax.to($('.intro'),1.3,{marginLeft:'50%', width:'50%', delay:3, ease:Strong.easeInOut});
                TweenMax.to($('.fcb-office'),1,{marginLeft:0, delay:3});


                TweenMax.delayedCall(4,function(){
                   $('.menu').css('display','block');
                   TweenMax.to($('.intro'),1,{marginLeft:'100%', width:0, delay:1, ease:Strong.easeIn});
                   TweenMax.to($('.fcb-office'),1,{marginLeft:'-100%', delay:1, ease:Strong.easeIn});
                    $('.menu-copy').css('padding-top',($('.menu-box').height()/2) - $('.menu-copy').height()/2);
                });
            });
        });
        
         $('.menu-box').mouseenter(function(e){
            var mc=$(this).find('.overlay');
            var img = $(mc).find('.menu-line');
            var title = $(mc).find('.menu-title');
            var desc = $(mc).find('p');
            TweenMax.to(mc,1,{marginLeft:'100%', ease:Strong.easeIn});
        });

        $('.menu-box').mouseleave(function(e){
            var mc=$(this).find('.overlay');
            var img = $(mc).find('.menu-line');
            var title = $(mc).find('.menu-title');
            var desc = $(mc).find('p');
            TweenMax.to(mc,1,{marginLeft:'0', ease:Strong.easeOut});
        });
        $('.menu-box').on( "tap", function( e ){
            
            Array.prototype.current = 0;
            
            $('.bx-wrapper').remove();
            
            $('.case-study').css('position','absolute');
            //$('.menu').css('position','fixed');
            var mc=$(this).find('.overlay');
            $('.case-study').css('background-color',$(mc).css('background-color').replace("0.70","0.95"));
            
            section=$(this).attr('id');
            if(section=="tab1")ajaxLoadPage('td.html');
            if(section=="tab2")ajaxLoadPage('kmart_tv.html');
            if(section=="tab3"){
                //$('.case-study').css('background-image',"url('images/intro-bg.jpg')");
                $('.left-arrow').css('display','none');
                $('.right-arrow').css('display','none');
                if(!$('.case-wrapper').hasClass('full-stretch')) $('.case-wrapper').toggleClass('full-stretch');
                ajaxLoadPage('team.html');
            }else{
                $('.left-arrow').css('display','block');
                $('.right-arrow').css('display','block');
                if($('.case-wrapper').hasClass('full-stretch')) $('.case-wrapper').toggleClass('full-stretch');
            }

            if(section=="tab4")ajaxLoadPage('kmart.html');
            
            /*$('.case-study').css('background-image',"url('images/intro-bg.jpg')");
                if(!$('.case-wrapper').hasClass('full-stretch')) $('.case-wrapper').toggleClass('full-stretch');
            if(!transforms){
                TweenMax.to($('.case-study'),1,{marginLeft:0, ease:Strong.easeOut});
            }else{
                if($('.case-study').hasClass('case-slide-out')) $('.case-study').removeClass('case-slide-out')
                else $('.case-study').addClass('case-slide-in');
            }   */
            
        });
        
        teamEvents = function(){
            $('.team').css('margin-top',($(window).height()/2)-($('.team').height()/2)-60);
            
            $('.person').on( "tap", function( e ){
                TweenMax.to($('.name-holder'),1,{opacity:0});
                $('.close-person-btn').css('display','block');
                selectedPerson = $(this);
                var mc = $(this).find('#person-box1');
                TweenMax.to($('.person').not(this),1,{width:'2%',ease:Strong.easeOut});
                if($('.bio-frame').css('opacity') <1){
                    TweenMax.to($('.bio-frame'),1,{opacity:1,delay:1});
                }else{
                    TweenMax.to($('.bio-frame'),1,{opacity:0});
                    TweenMax.to($('.bio-frame'),1,{opacity:1,delay:1});
                }
                TweenMax.to($('.bio-frame'),1,{opacity:1, delay:0.7});
                TweenMax.to($(this),1,{width:'90%', onUpdate:function(){$('.bio-box').css('margin-top',(460/2)-($(mc).height())/2);}, ease:Strong.easeIn, delay:0.7});
            });

            $('.close-person-btn').on( "tap", function( e ){
                TweenMax.to($('.name-holder'),1,{opacity:1,delay:0.75});
               TweenMax.to($('.bio-frame'),1,{opacity:0});
                $('.close-person-btn').css('display','none');
                TweenMax.to($('.person').not(selectedPerson),1,{width:'16.666%', ease:Strong.easeOut,delay:1});
                TweenMax.to($(selectedPerson),1,{width:'16.666%', ease:Strong.easeOut, delay:0});
            });
        }
        
        $('.close-btn').click(function( e ){
            modalOpen=false;
            $('.menu').css('position','absolute');
            $('.case-study').css('position','fixed');
            if(!transforms)TweenMax.to($('.case-study'),1,{marginLeft:'100%', ease:Strong.easeIn});
            else $('.case-study').addClass('case-slide-out');
        });
        
        $('.right-arrow').on( "tap", function( e ){
            e.preventDefault();
            if(modalOpen)TweenMax.to($('.case-wrapper'),0.5,{marginTop:100,ease:Strong.easeIn, opacity:0});
            TweenMax.delayedCall(0.7,function(){
                if(section=="tab1")ajaxLoadPage(creative_work.next()+".html");
                if(section=="tab2")ajaxLoadPage(tv_work.next()+".html");
                if(section=="tab4")ajaxLoadPage(case_studies.next()+".html");
            });
        });
        
        $('.left-arrow').on( "tap", function( e ){
            e.preventDefault();
            if(modalOpen)TweenMax.to($('.case-wrapper'),0.5,{marginTop:100,ease:Strong.easeIn, opacity:0});
            TweenMax.delayedCall(0.7,function(){
                if(section=="tab1")ajaxLoadPage(creative_work.prev()+".html");
                if(section=="tab2")ajaxLoadPage(tv_work.prev()+".html");
                if(section=="tab4")ajaxLoadPage(case_studies.prev()+".html");
            });
        });
        

    }
    
    
    window.onresize = function(event) {
        if($(window).height() >= 600){
            $('.menu-box').css('height',$(window).height()/4);
        }
        if($(window).width() <=1024){
            $('.team').css('margin-top','0px');
        }else{
            
        }
        if(!touch){
            $('.team').css('margin-top',($(window).height()/2)-($('.team').height()/2)-60);
        }
        $('.menu-copy').css('padding-top',($('.menu-box').height()/2) - $('.menu-copy').height()/2);
        $('.person').each(function(){
            $(this).find('.bio-box').css('margin-top',(460/2)-$(this).find('#person-box1').height()/2);
        });
        $('.case-wrapper').eq(0).find('.bio-box').css('margin-top',(460/2)-$('.case-wrapper').eq(0).find('#person-box1').height()/2);
    };
    
    if($(window).height() >= 600){
            $('.menu-box').css('height',$(window).height()/4);
        }
        
    
    function ajaxLoadPage(page){
        $( ".case-wrapper" ).load( page , function( response, status, xhr ) {
            if ( status == "error" ) {
                
            }
            if ( status == "success" ) {
                if($('.case-wrapper').css('opacity') <1) TweenMax.to($('.case-wrapper'),1,{marginTop:0,opacity:1});
                
                if(!transforms){
                    TweenMax.to($('.case-study'),1,{marginLeft:0, ease:Strong.easeOut});
                }else{
                    if($('.case-study').hasClass('case-slide-out')) $('.case-study').removeClass('case-slide-out')
                    else $('.case-study').addClass('case-slide-in');
                }

                $('.bxslider').bxSlider({
                    slideWidth:692,
                    controls:true,
                    video:true,
                    adaptiveHeight: true
                });
                    
                modalOpen=true;
                
                if(!touch)teamEvents();
                TweenMax.delayedCall(0,function(){
                    $('.person').each(function(){
                        $(this).find('.bio-box').css('margin-top',(460/2)-$(this).find('#person-box1').height()/2);
                    });
                });
                if(!touch){
                    TweenMax.to($('.person'),0,{width:'10.666%'});
                    TweenMax.to($('.person'),0,{width:'16.666%'});
                    $('.bio-frame').css('opacity','0');
                }
                $('.case-wrapper').eq(0).find('.bio-box').css('margin-top',(460/2)-$('.case-wrapper').eq(0).find('#person-box1').height()/2);
            }
        });
    }
    
    Array.prototype.next = function() {
        if (!((this.current + 1) in this)) this.current=-1
        return this[++this.current];
    };
    
    Array.prototype.prev = function() {
        if (!((this.current - 1) in this)) this.current = this.length;
        return this[--this.current];
    };
    
    Array.prototype.current = 0;
});